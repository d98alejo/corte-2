from django.db import models
from django.contrib.auth.models import User
# Create your models here.



class Estudiante(models.Model):
    fortalezas = models.CharField(max_length=45,null=True)
    herramientas = models.CharField(max_length=45,null=True)
    user = models.ForeignKey(
        User,
        related_name='estudiantes',
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.fortalezas

    class Meta():
        app_label: 'app'

class Empresa(models.Model):
    razon_social = models.CharField(max_length=45,null=False)
    ciudad = models.CharField(max_length=45,null=False)
    nombre_contacto = models.CharField(max_length=45,null=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        User,
        related_name='empresas',
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.razon_social

    class Meta():
        app_label: 'app'



class Coordinador(models.Model):
    celular = models.CharField(max_length=45,null=True)
    user=models.ForeignKey(
        User,
        related_name='coordinadores',
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.celular

    class Meta():
        app_label: 'app'

class Configuracion(models.Model):
    semestre = models.CharField(max_length=45,null=True)

    def __str__(self):
        return self.semestre

    class Meta():
        app_label: 'app'

class Convocatoria(models.Model):
    nombre = models.CharField(max_length=45,null=False)
    descripcion = models.CharField(max_length=45,null=True)
    fecha_inicial = models.DateTimeField(auto_now_add=False,auto_now=False,null=True)
    fecha_final = models.DateTimeField(auto_now_add=False,auto_now=False,null=True)
    
    cerrada = models.BooleanField(default=True) 
    empresa=models.ForeignKey(
        Empresa,
        related_name='convocatorias',
        on_delete=models.PROTECT
    )
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.nombre

    class Meta():
        app_label: 'app'

class Postulacion(models.Model):
    estudiante= models.ForeignKey(
        Estudiante,
        related_name='postulaciones',
        on_delete=models.PROTECT
    )
    convocatoria=models.ForeignKey(
        Convocatoria,
        related_name='postulaciones',
        on_delete=models.PROTECT
    )
    semestre = models.CharField(max_length=45,null=True)
    rechazada = models.BooleanField(default=True) 
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.semestre

    class Meta():
        app_label: 'app'
    




