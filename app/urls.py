from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    #Inicio e inscripcion
    path('', views.paginadeinicio, name='paginadeinicio'), 
    path('iniciarsesion/', views.iniciarsesion, name='iniciarsesion'), #El primero es el que va a el views,el segundo es el nombre de la url
    path('salir/', views.salir, name='salir'),
    path('iniciarsesion_post/', views.post_iniciarsesion, name='post_iniciarsesion'),
    path('acercade/', views.acercade, name='acercade'),
    path('iniciarsesion/error_acceso', views.iniciarsesion_error, name='iniciarsesion_error'),
    path('error', views.error, name='error'),
    #Empresa
    path('crearempresa/', views.crearempresa, name='crearempresa'),
    path('crearempresa/f', views.crearempresa_f, name='crearempresa_f'),
    #path('crearempresa/final', views.crearempresa_final, name='crearempresa_final'),#Probar
    path('inicioempresa/', views.inicio_empresa, name='inicio_empresa'),
    path('inicioempresa/crear_conv', views.crear_conv, name='crear_conv'),
    path('inicioempresa/crear_convf', views.crear_conv_f, name='crear_conv_f'),
    #path('inicioempresa/crear_conv/final', views.crear_conv_final, name='cons_conv_final'),
    path('inicioempresa/lista_conv/', views.lista_conv_empr, name='lista_conv_empr'),
    path('inicioempresa/lista_conv/cons/<int:id>/abierta', views.cons_conv_empr_a, name='cons_conv_empr_a'),
    path('inicioempresa/lista_conv/cons/<int:id>/cerrada', views.cons_conv_empr_c, name='cons_conv_empr_c'),
    path('inicioempresa/lista_conv/cons/<int:id>/postulacion', views.editar_postulacion, name='editar_postulacion'),
    path('inicioempresa/lista_conv/conv/<int:id>/editar', views.conv_editar, name='conv_editar'),
    path('inicioempresa/lista_conv/conv/<int:id>/editar_varios', views.conv_editar_muchos, name='conv_editar_muchos'),
    path('inicioempresa/lista_conv/cons/<int:id>/editar_0', views.conv_editar_0, name='conv_editar_0'),
    path('inicioempresa/lista_conv/cons/<int:id>/editar_estado', views.conv_editar_estado, name='conv_editar_estado'),
    path('inicioempresa/cons_perf_est_empr/', views.cons_perf_est_empr, name='cons_perf_est_empr'),

    #Estudiante
    path('inicioestudiante/', views.inicio_estudiante, name='inicio_estudiante'),
    path('inicioestudiante/lista_conv_est', views.lista_conv_est, name='lista_conv_est'),
    path('inicioestudiante/cons_conv/<int:id>/aplicar', views.cons_conv_est_aplicar, name='cons_conv_est_aplicar'),
    path('inicioestudiante/cons_conv/<int:id>/aplicar_form', views.est_aplicar_form, name='est_aplicar_form'),
    #path('inicioestudiante/cons_conv/<int:id>/final', views.cons_conv_est_final, name='cons_conv_est_final'),
    path('inicioestudiante/lista_post_est/', views.lista_post_est, name='lista_post_est'),
    path('inicioestudiante/lista_post_est/<int:id>/aceptada', views.post_est_acep, name='post_est_acep'),
    path('inicioestudiante/lista_post_est/<int:id>/rechazada', views.post_est_rech, name='post_est_rech'),
    path('inicioestudiante/edit_perf_est', views.edit_perf_est, name='edit_perf_est'),
    path('inicioestudiante/edit_perf_est/final', views.edit_perf_est_final, name='edit_perf_est_final'),
    #Error por si consulta la conv y/o lista de otro:
    #path('inicioestudiante/lista_post_est/<int:id>/error', views.lista_post_est_error, name='lista_post_est_error'),
    #path('inicioestudiante/lista_post_est/info/<int:id>/error', views.info_post_est_error, name='info_post_est_error'),
    #path('inicioestudiante/edit_perf_est/<int:id>/error', views.edit_perf_est_error, name='edit_perf_est_error'),

    #Coordinador

    path('iniciocoord/', views.inicio_coord, name='inicio_coord'),
    path('iniciocoord/lista_empr', views.lista_empr, name='lista_empr'),
    path('iniciocoord/lista_empr/cons_empr/<int:id>/', views.cons_empr, name='cons_empr'),
    path('iniciocoord/lista_conv', views.lista_conv, name='lista_conv'),
    path('iniciocoord/lista_conv/cons_conv/<int:id>/abierta', views.cons_conv_a, name='cons_conv_a'),
    path('iniciocoord/lista_conv/cons_conv/<int:id>/cerrada', views.cons_conv_c, name='cons_conv_c'),
    path('iniciocoord/crear_est', views.crear_est, name='crear_est'),
    path('iniciocoord/crear_estf', views.crear_est_f, name='crear_est_f'),
    path('iniciocoord/cons_est_perf', views.cons_est_perf, name='cons_est_perf'),
    path('iniciocoord/cons_est_pract', views.cons_est_pract, name='cons_est_pract'),
    path('iniciocoord/edit_sem', views.edit_sem, name='edit_sem'),
    path('iniciocoord/edit_semf', views.edit_sem_f, name='edit_sem_f'),
    
]