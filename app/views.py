from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import User, Estudiante, Configuracion, Empresa, Convocatoria, Postulacion, Coordinador
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

#***PAGINAS DE INICIO***#
def paginadeinicio(request):
    return render(request,'app/paginadeinicio.html')

def acercade(request):
    return render(request, 'app/info.html' )

def iniciarsesion(request):

    return render(request, 'app/iniciarsesion.html' )

def post_iniciarsesion(request):
    username = request.POST['username']
    password = request.POST['password']
    
    usuario = authenticate(username=username,password=password)
    if usuario is not None:
        login(request, usuario)
        try:
            empresa = Empresa.objects.get(user=usuario)
            print(request.user.id)
            print(empresa.id)
            print(empresa)
            print(request.user)
            print(usuario)
            return redirect('app:inicio_empresa')
        except:
            pass
        try:
            estudiante = Estudiante.objects.get(user=usuario)
            print(request.user.id)
            print(estudiante.id)
            print(estudiante)
            print(request.user)
            print(usuario)
            return redirect('app:inicio_estudiante')
        except:
            pass 
        try:
            coordinador = Coordinador.objects.get(user=usuario)
            return redirect('app:inicio_coord')
        except:
            pass 
    else:
        return render(request, 'app/errordeacceso.html')

def iniciarsesion_error(request):


    return render(request, 'app/errordeacceso.html')

def error(request):


    return render(request, 'app/error.html')


def salir(request):
    logout(request)
    return render(request, 'app/iniciarsesion.html' )


def crearempresa(request):
    return render(request, 'app/crearempresa.html' )


def crearempresa_f(request):
    usuario = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']
    razon_social = request.POST['razon_social']
    ciudad = request.POST['ciudad']
    nombre_contacto = request.POST['nombre_contacto']
    usuario_empresa = User()
    usuario_empresa.username=usuario
    usuario_empresa.set_password(password)
    usuario_empresa.email=email
    usuario_empresa.save()
    nueva_empr = Empresa()
    nueva_empr.razon_social = razon_social
    nueva_empr.ciudad=ciudad
    nueva_empr.nombre_contacto=nombre_contacto
    nueva_empr.user=usuario_empresa
    nueva_empr.save()
    return redirect('app:iniciarsesion')
    

#***FIN DE PAGINAS DE INICIO***#

#***INICIO ESTUDIANTE***#
@login_required

def inicio_estudiante(request):
    
    try:
        estudiante = Estudiante.objects.get(user_id=request.user.id)
        return render(request,'app/inicioestudiante.html')  
    except:
        return render(request, 'app/error.html')

@login_required
def lista_conv_est(request):
    try: 
        estudiante = Estudiante.objects.get(user_id=request.user.id)
        lista_convocatoria = Convocatoria.objects.all()
        contexto={
        'est_convocatorias':lista_convocatoria
        }
        return render(request, 'app/listaconvest.html' ,contexto)
    except:
        return render(request, 'app/error.html')

@login_required
def est_aplicar_form(request,id):
  
    usuario_activo = request.user
    estudiante = Estudiante.objects.get(user=usuario_activo)
    numero_convocatoria = Convocatoria.objects.get(id=id)
    nueva_postulacion = Postulacion()
    nueva_postulacion.semestre=Configuracion.objects.get(id=1)
    nueva_postulacion.convocatoria_id=numero_convocatoria.id
    nueva_postulacion.estudiante_id=estudiante.id
    nueva_postulacion.save()
    return redirect('app:lista_post_est')

@login_required
def cons_conv_est_aplicar(request,id):
    try:
        estudiante = Estudiante.objects.get(user_id=request.user.id)
        usuario_activo = request.user
        estudiante = Estudiante.objects.get(user=usuario_activo)
    
        cantidad_de_postulaciones = Postulacion.objects.filter(estudiante=estudiante.id,convocatoria_id=id).count()
    
        consulta_convocatoria = Convocatoria.objects.get(id=id)
        contexto = { 
        'consulta3':consulta_convocatoria,
        'cantidad':cantidad_de_postulaciones    
        }
        return render(request,'app/consultaconvabierta.html',contexto)
    except:
        return render(request, 'app/error.html')



@login_required
def lista_post_est(request):
    try:
        estudiante = Estudiante.objects.get(user_id=request.user.id)
        usuario_activo = request.user
        estudiante = Estudiante.objects.get(user=usuario_activo)
        postulacion = Postulacion.objects.filter(estudiante_id=estudiante.id).distinct('convocatoria_id')
        contexto={
            'postulaciones':postulacion
        }
        return render(request, 'app/listapostest.html',contexto )
    except:
        return render(request, 'app/error.html')


@login_required
def post_est_acep(request,id): 
    try:
        estudiante = Estudiante.objects.get(user_id=request.user.id)
        postulaciones = Postulacion.objects.get(id=id)
        if estudiante.id == postulaciones.estudiante_id:
            contexto={
                'postulacion_aceptada':postulaciones
            }
            return render(request, 'app/infopostacepest.html',contexto)
        else:
            return render(request, 'app/error.html')
    except:
        return render(request, 'app/error.html')

@login_required
def post_est_rech(request,id): 
    try:
        estudiante = Estudiante.objects.get(user_id=request.user.id)
        postulaciones = Postulacion.objects.get(id=id)
        if estudiante.id == postulaciones.estudiante_id:
            contexto={
                'postulacion_rechazada':postulaciones
            }
            return render(request, 'app/infopostrechest.html',contexto)
        else:
            return render(request, 'app/error.html')
    except:
        return render(request, 'app/error.html')


@login_required
def edit_perf_est(request):
    try:
        estudiante = Estudiante.objects.get(user_id=request.user.id)
        contexto = { 
        'est':estudiante
        }
        return render(request,'app/editarperfilest.html',contexto)
    except:
        return render(request, 'app/error.html')

@login_required
def edit_perf_est_final(request):

    estudiante_actual=Estudiante.objects.get(user_id=request.user.id)
    estudiante_actual_correo=User.objects.get(id=request.user.id)
    fortalezas = request.POST['fortalezas']
    herramientas = request.POST['herramientas']
    email = request.POST['email'] 
    estudiante_actual_correo.email=email
    estudiante_actual.fortalezas=fortalezas
    estudiante_actual.herramientas=herramientas
    estudiante_actual.save()
    estudiante_actual_correo.save()

    return redirect('app:inicio_estudiante')
#***FIN INICIO ESTUDIANTE***#


#***INICIO EMPRESA***#
@login_required
def inicio_empresa(request):

    try:
        empresa = Empresa.objects.get(user_id=request.user.id)
        return render(request,'app/inicioempresa.html')  
    except:
        return render(request, 'app/error.html')
    


@login_required
def crear_conv(request):
    try:
        empresa = Empresa.objects.get(user_id=request.user.id)
        return render(request,'app/crearconvempresa.html')
    except:
        return render(request, 'app/error.html')




@login_required
def crear_conv_f(request):

    usuario_activo = request.user
    empresa = Empresa.objects.get(user=usuario_activo)
    nombre = request.POST['nombre']
    descripcion = request.POST['descripcion']
    fecha_inicial = request.POST['fecha_inicial']
    fecha_final = request.POST['fecha_final']
    nueva_conv = Convocatoria()
    nueva_conv.nombre = nombre
    nueva_conv.descripcion = descripcion
    nueva_conv.fecha_inicial = fecha_inicial
    nueva_conv.fecha_final = fecha_final
    nueva_conv.empresa_id=empresa.id #empresa en el sistema
    nueva_conv.save()
    

    return redirect('app:lista_conv_empr')

@login_required
def lista_conv_empr(request): #P.1
    try:
        empresa = Empresa.objects.get(user_id=request.user.id)
        todas_convocatorias = Convocatoria.objects.filter(empresa__user=request.user.id) #Mostrar
        datos = []
        for c in todas_convocatorias:
            cantidad = Postulacion.objects.filter(convocatoria_id=c.id).distinct('estudiante_id').count()
            dato = {
                'id':c.id,
                'cantidad':cantidad,
                'nombre':c.nombre,
                'descripcion':c.descripcion,
                'fecha_inicial':c.fecha_inicial,
                'fecha_final':c.fecha_final,
                'cerrada':c.cerrada,
                'email':c.empresa.user.email
            }
            datos.append(dato)
        contexto = { 
        'convocatorias':datos     
        }
        return render(request,'app/listaconvempresa.html',contexto)
    except:
        return render(request, 'app/error.html')

@login_required
def cons_conv_empr_c(request,id):
    try:
        empresa = Empresa.objects.get(user_id=request.user.id)
        consulta_convocatoria = Convocatoria.objects.get(id=id)
        lista_postulantes = Postulacion.objects.filter(convocatoria_id=id).distinct('estudiante_id')
        if empresa.id == consulta_convocatoria.empresa_id:
            contexto = { 
            'consulta':consulta_convocatoria,
            'postulantes':lista_postulantes
            }
            return render(request,'app/consconvempresacerrada.html',contexto)
        else:
            return render(request, 'app/error.html')
    except:
        return render(request, 'app/error.html')

@login_required
def cons_conv_empr_a(request,id):
    try:
        empresa = Empresa.objects.get(user_id=request.user.id)
        consulta_convocatoria = Convocatoria.objects.get(id=id)
        postulaciones = Postulacion.objects.filter(convocatoria_id=id).distinct('estudiante_id')
        if empresa.id == consulta_convocatoria.empresa_id:
            contexto = { 
            'consulta':consulta_convocatoria,
            'postulaciones':postulaciones    
            }
            return render(request,'app/consconvempresa.html',contexto)
        else:
            return render(request, 'app/error.html')
    except:
        return render(request, 'app/error.html')

@login_required
def editar_postulacion(request,id):

    """  postulacion = Postulacion.objects.filter(convocatoria_id=id)
    for post in postulacion:
        rechazada = request.POST['rechazada']
        post.rechazada=rechazada
        post.save() """
    
    rechazada = request.POST['rechazada']
    postulacion = request.POST['postulacion']
    postulaciones = Postulacion.objects.get(id=postulacion)
    postulaciones.rechazada=rechazada
    postulaciones.id=postulacion
    postulaciones.save()

    
    return redirect('app:cons_conv_empr_a',id)




@login_required
def conv_editar(request,id): #POST
    try:
        empresa = Empresa.objects.get(user_id=request.user.id)
        editar_convocatoria = Convocatoria.objects.get(id=id)
        if empresa.id == editar_convocatoria.empresa_id:
            contexto = { 
            'editar':editar_convocatoria
            }
            return render(request,'app/editconvempresa0ins.html',contexto)
        else:
            return render(request, 'app/error.html')
    except:
        return render(request, 'app/error.html')

@login_required
def conv_editar_muchos(request,id):
    try:
        empresa = Empresa.objects.get(user=request.user)
        print(empresa.id)
        editar_convocatoria = Convocatoria.objects.get(id=id)
        print(editar_convocatoria.empresa_id)
        if empresa.id == editar_convocatoria.empresa_id:
            contexto = {
            'editar':editar_convocatoria
            }
            return render(request,'app/editconvempresa1ins.html',contexto)
        else:
            return render(request, 'app/error.html')
    except:
        return render(request, 'app/error.html')
    

@login_required
def conv_editar_0(request,id): #Formulario
    estado = Convocatoria.objects.get(id=id)
    nombre = request.POST['nombre']
    descripcion = request.POST['descripcion']
    fecha_inicial = request.POST['fecha_inicial']
    fecha_final = request.POST['fecha_final']
    estado.nombre=nombre
    estado.descripcion=descripcion
    estado.fecha_inicial = fecha_inicial
    estado.fecha_final = fecha_final
    estado.save()
    

    return redirect('app:lista_conv_empr')


@login_required
def conv_editar_estado(request,id):
    estado = Convocatoria.objects.get(id=id)
    cerrada = request.POST['cerrada']
    estado.cerrada=cerrada
    estado.save()
  

    return redirect('app:lista_conv_empr')

@login_required
def cons_perf_est_empr(request): #P.5

    try:
        empresa = Empresa.objects.get(user=request.user.id)
        postulaciones_empresa = Postulacion.objects.filter(convocatoria__empresa=empresa).distinct('estudiante_id') #Otra forma de hacer el filtro,con parametros que no tiene la clase          
        contexto = { 
        'estudiantes_empresa':postulaciones_empresa
        }
        return render(request,'app/consperfilestempresa.html',contexto)
    except:
        return render(request, 'app/error.html')
#*** FIN PAGINAS EMPRESA***#


#***INICIO COORDINADOR***#

@login_required
def inicio_coord(request):
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        return render(request,'app/iniciocoordinador.html')  
    except:
        return render(request, 'app/error.html')
    

@login_required
def lista_empr(request):
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        lista_empresas = Empresa.objects.all()
        contexto={
        'lista_de_empresas':lista_empresas
        }
        return render(request,'app/listadeempresas.html',contexto)
    except:
        return render(request, 'app/error.html')

@login_required
def lista_conv(request):
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        lista_conv_coord = Convocatoria.objects.all()
        datos = []
        for c in lista_conv_coord:
            cantidad = Postulacion.objects.filter(convocatoria_id=c.id).distinct('estudiante_id').count()
            dato = {
                'id':c.id,
                'cantidad':cantidad,
                'nombre':c.nombre,
                'descripcion':c.descripcion,
                'fecha_inicial':c.fecha_inicial,
                'fecha_final':c.fecha_final,
                'cerrada':c.cerrada,
                'email':c.empresa.user.email
            }
            datos.append(dato)
        contexto = { 
        'coord_convocatorias':datos     
        }
        return render(request,'app/listaconvcoordinador.html',contexto)
    except:
        return render(request, 'app/error.html')



@login_required
def cons_empr(request,id): 
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        consulta_empresa = Empresa.objects.get(id=id)
        convocatorias_empresa = Convocatoria.objects.filter(empresa_id=id) 
        datos =[]
        for c in convocatorias_empresa:
            cantidad_postulados = Postulacion.objects.filter(convocatoria_id=c.id).distinct('estudiante_id').count()

            dato ={
                'cantidad':cantidad_postulados,
                'nombre':c.nombre,
                'fecha_inicial':c.fecha_inicial,
                'fecha_final':c.fecha_final,
                'cerrada':c.cerrada
            }
            datos.append(dato)
        contexto = { 
        'empresa':consulta_empresa,
        'convocatorias_de_empresa':datos  
        }
        return render(request,'app/consultaemprescoord.html',contexto)
    except:
        return render(request, 'app/error.html')


@login_required
def crear_est(request):
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        return render(request,'app/crearestudiante.html')  
    except:
        return render(request, 'app/error.html')
    

@login_required
def cons_conv_a(request,id):
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        consulta_convocatoria = Convocatoria.objects.get(id=id)
        lista_postulantes = Postulacion.objects.filter(convocatoria_id=id).distinct('estudiante_id')
        contexto = { 
        'consulta_conv':consulta_convocatoria,
        'postulantes':lista_postulantes       
        }
        return render(request,'app/consconvcoordinadorabierta.html',contexto)
    except:
        return render(request, 'app/error.html')

@login_required
def cons_conv_c(request,id):
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        consulta_convocatoria = Convocatoria.objects.get(id=id)
        lista_postulantes = Postulacion.objects.filter(convocatoria_id=id).distinct('estudiante_id')
        contexto = { 
        'consulta_conv':consulta_convocatoria,
        'postulantes':lista_postulantes    
        }
        return render(request,'app/consconvcoordinadorcerrada.html',contexto)
    except:
        return render(request, 'app/error.html')

@login_required
def cons_est_perf(request):
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        lista_estudiantes = Estudiante.objects.all()
        datos = []
        for c in lista_estudiantes:
            cantidad = Postulacion.objects.filter(estudiante=c,rechazada=False).count()
            dato = {
                'id':c.id,
                'first_name':c.user.first_name,
                'last_name':c.user.last_name,
                'herramientas':c.herramientas,
                'fortalezas':c.fortalezas,
                'email':c.user.email,
                'rechazada': 'Si' if cantidad >0 else 'No'
            }
            datos.append(dato)
    
        contexto = { 
        'estudiantes':datos
        }
        return render(request,'app/consperfilestcoord.html',contexto)

    except:
        return render(request, 'app/error.html')

@login_required
def cons_est_pract(request):
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        estudiante = Postulacion.objects.filter(rechazada=False).order_by('semestre') #no me deja poner distinct por eso sale repetidos
        contexto = { 
            'estudiantespractica':estudiante
        }
        return render(request,'app/consestenpractica.html',contexto)
    except:
        return render(request, 'app/error.html')

@login_required
def edit_sem(request):
    try:
        coordinador = Coordinador.objects.get(user_id=request.user.id)
        semestre = Configuracion.objects.latest('id')
        contexto = { 
            'semestres':semestre
        }
        return render(request,'app/editarsemestre.html',contexto)
    except:
        return render(request, 'app/error.html')


@login_required
def edit_sem_f(request):
    semestre = request.POST['semestre']
    """ semestre_act = Configuracion(semestre=semestre) """
    semestre_act=Configuracion.objects.get(id=1)
    semestre_act.semestre=semestre
    semestre_act.save()
    return redirect('app:edit_sem')

@login_required
def crear_est_f(request):
    username = request.POST['username']
    password = request.POST['password']
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']
    email = request.POST['email']
    usuario_estudiante = User()
    usuario_estudiante.username=username
    usuario_estudiante.set_password(password) 
    usuario_estudiante.first_name=first_name
    usuario_estudiante.last_name=last_name
    usuario_estudiante.email=email
    usuario_estudiante.save()
    nuevo_est = Estudiante()
    nuevo_est.fortalezas=''
    nuevo_est.herramientas=''
    nuevo_est.user=usuario_estudiante
    nuevo_est.save()
    return redirect('app:cons_est_perf')
#*** FIN PAGINAS COORDINADOR***#